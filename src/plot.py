import matplotlib.pyplot as plt
import numpy as np
import csv

x0 = []
y0 = []
x1 = []
y1 = []
x2 = []
y2 = []
x3 = []
y3 = []

bar_width = 0.19
line_width = 0.5

with open('../drl/computational/summary_honest.csv', 'r') as f:
    reader = list(csv.reader(f))
    for row in reader[1:]:
        x0.append(float(row[0]) + bar_width * 0)
        y0.append(float(row[6]))

with open('../drl/computational/summary_pac_honest.csv', 'r') as f:
    reader = list(csv.reader(f))
    for row in reader[1:]:
        x0.append(float(row[0]) + 40 + bar_width * 0)
        y0.append(float(row[6]))

with open('../drl/computational/summary_ambiguity.csv', 'r') as f:
    reader = list(csv.reader(f))
    for row in reader[1:]:
        x1.append(float(row[0]) + bar_width * 1)
        y1.append(float(row[6]))

with open('../drl/computational/summary_pac_ambiguity.csv', 'r') as f:
    reader = list(csv.reader(f))
    for row in reader[1:]:
        x1.append(float(row[0]) + 40 + bar_width * 1)
        y1.append(float(row[6]))

with open('../drl/computational/summary_irrational_l.csv', 'r') as f:
    reader = list(csv.reader(f))
    for row in reader[1:]:
        x2.append(float(row[0]) + bar_width * 3)
        y2.append(float(row[6]))

with open('../drl/computational/summary_pac_irrational_l.csv', 'r') as f:
    reader = list(csv.reader(f))
    for row in reader[1:]:
        x2.append(float(row[0]) + 40 + bar_width * 3)
        y2.append(float(row[6]))

with open('../drl/computational/summary_irrational_m.csv', 'r') as f:
    reader = list(csv.reader(f))
    for row in reader[1:]:
        x3.append(float(row[0]) + bar_width * 2)
        y3.append(float(row[6]))

with open('../drl/computational/summary_pac_irrational_m.csv', 'r') as f:
    reader = list(csv.reader(f))
    for row in reader[1:]:
        x3.append(float(row[0]) + 40 + bar_width * 2)
        y3.append(float(row[6]))


def group_by_goal(src_array):
    goal_3 = []
    goal_4 = src_array[40:]
    goal_5 = []
    for i in range(0, 40, 8):
        goal_3.extend(src_array[i:i+4])
        goal_5.extend(src_array[i+4:i+8])
    ret_array = [float(sum(goal_3)) / len(goal_3), float(sum(goal_4)) / len(goal_4), float(sum(goal_5)) / len(goal_5)]
    return ret_array

sum_x0 = [1 + bar_width * 0, 2 + bar_width * 0, 3 + bar_width * 0]
sum_x1 = [1 + bar_width * 1, 2 + bar_width * 1, 3 + bar_width * 1]
sum_x2 = [1 + bar_width * 2, 2 + bar_width * 2, 3 + bar_width * 2]
sum_x3 = [1 + bar_width * 3, 2 + bar_width * 3, 3 + bar_width * 3]

sum_y0 = group_by_goal(y0)
sum_y1 = group_by_goal(y1)
sum_y2 = group_by_goal(y2)
sum_y3 = group_by_goal(y3)

plt.figure()
# plt.bar(x0, y0, width=bar_width, linewidth=line_width, label='Honest')
# plt.bar(x1, y1, width=bar_width, linewidth=line_width, label='Ambiguity')
# plt.bar(x2, y2, width=bar_width, linewidth=line_width, label='IR_0.3')
# plt.bar(x3, y3, width=bar_width, linewidth=line_width, label='IR_0.5')

plt.bar(sum_x0, sum_y0, width=bar_width, linewidth=line_width, label='Honest')
plt.bar(sum_x1, sum_y1, width=bar_width, linewidth=line_width, label='Ambiguity')
plt.bar(sum_x2, sum_y2, width=bar_width, linewidth=line_width, label='IR_0.3')
plt.bar(sum_x3, sum_y3, width=bar_width, linewidth=line_width, label='IR_0.5')

plt.legend(loc='best', ncol=4)
plt.xticks(np.arange(1.3, 4.3, 1), ['3-goal', '4-goal', '5-goal'])
axes = plt.gca()
axes.set_ylim([0.0, 1.0])
plt.show()
